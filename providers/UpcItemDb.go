package providers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type UpcItemDb struct {
}

type Response struct {
	Code string `json:"code"`
	Message string `json:"message"`
	Total int `json:"total"`
	Offset int `json:"offset"`
	Items []ItemResponse `json:"items"`
}

type ItemResponse struct {
	Ean string `json:"ean"`
	Upc string `json:"upc"`
	Isbn string `json:"isbn"`
	Title string `json:"title"`
	Description string `json:"description"`
	Brand string `json:"brand"`
	Publisher string `json:"publisher"`
	Model string `json:"model"`
	Color string `json:"color"`
	Size string `json:"size"`
	Weight string `json:"weight"`
	Category string `json:"category"`
	Images []string `json:"images"`
	Asin string `json:"asin"`
	Elid string `json:"elid"`
}

func (u UpcItemDb) GetItem(barcode string) *Item {
	//TODO: we need to be able to handle 10 digit ISBN barcodes here
	if len(barcode) != 12 && len(barcode) != 13 {
		return nil
	}

	url := fmt.Sprintf("https://api.upcitemdb.com/prod/trial/lookup?upc=%s", barcode)
	request, err := http.NewRequest("GET", url, nil)
	request.Header.Add("Accept", "application/json")

	httpResponse, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Printf("[ERROR] UpcItemDb request failed with err: %s", err)
		return nil
	}

	response := &Response{}
	err = json.NewDecoder(httpResponse.Body).Decode(response)
	if err != nil {
		log.Printf("[ERROR] failed to decode response from UpcItemDb: %s", err)
		return nil
	}

	if response.Code != "OK" {
		log.Printf("[ERROR] UpcItemDb request failed with err=: %s", err)
	}

	responseItem, itemType := getItem(response)
	switch itemType {
	case "book":
		return &Item{
			Upc: responseItem.Upc,
			Isbn13: responseItem.Isbn,
			Title: getBookTitle(responseItem),
			Creator: getBookCreator(responseItem),
			Image: getImage(responseItem),
			ItemType: 0,
		}
	case "dvd":
		return &Item{
			Title: getVideoTitle(responseItem.Title),
			Upc: responseItem.Upc,
			Creator: responseItem.Brand,
			Image: getImage(responseItem),
			ItemType: 1,
		}
	case "vhs":
		return &Item{
			Title: getVideoTitle(responseItem.Title),
			Upc: responseItem.Upc,
			Creator: responseItem.Brand,
			Image: getImage(responseItem),
			ItemType: 2,
		}
	default:
		return nil
	}
}

func getVideoTitle(title string) string {
	metadata := strings.Index(title, "[")
	if(metadata < 5) {
		return title
	}

	return title[:metadata-1]
}

func getImage(item ItemResponse) string {
	if len(item.Images) > 0 {
		return item.Images[0]
	}
	return ""
}

func getBookTitle(item ItemResponse) string {
	return strings.Split(item.Title, " -")[0]
}

func getBookCreator(item ItemResponse) string {
	split := strings.Split(item.Title, "-")
	if len(split) < 2 {
		return ""
	}
	metadata := split[1]
	endOfAuthor := strings.LastIndex(metadata, "(")
	startOfAuthor := strings.LastIndex(metadata, "by ")
	if startOfAuthor < 0 || endOfAuthor < 0 {
		return ""
	}
	return metadata[startOfAuthor+3:endOfAuthor-1]
}

func getItem(response *Response) (ItemResponse, string) {
	if response.Total == 0 {
		return ItemResponse{},""
	}

	for _, item := range response.Items {
		if strings.HasSuffix(item.Category, "Books") {
			return item, "book"
		}
		if strings.HasSuffix(item.Category, "DVDs & Videos") {
			if strings.Contains(item.Title, "(dvd") {
				return item, "dvd"
			}
			return item, "vhs"
		}
	}
	return ItemResponse{},""
}

func (u UpcItemDb) GetName() string {
	return "UpcItemDb"
}

func NewUpcItemDb() *UpcItemDb {
	return &UpcItemDb{
	}
}


//Sample Error
//{"code":"INVALID_UPC","message":"Not a valid UPC code."}

/*
BOOK
GET https://api.upcitemdb.com/prod/trial/lookup?upc=9780380788620
{
  "code": "OK",
  "total": 1,
  "offset": 0,
  "items": [
    {
      "ean": "9780380788620",
      "title": "Cryptonomicon - by Neal Stephenson (Paperback)",
      "description": "",
      "isbn": "9780380788620",
      "publisher": "William Morrow Paperbacks",
      "category": "Media > Books",
      "images": [
        "https://covers1.booksamillion.com/covers/bam/0/38/078/862/0380788624.jpg",
        "https://images.BetterWorldBooks.com/038/Cryptonomicon-9780380788620.jpg",
        "https://i5.walmartimages.com/asr/112c9203-510b-4560-b827-6cec6d54ca0f.e387cb8c863227f59b3a3e2afd34107e.jpeg?odnHeight=450&odnWidth=450&odnBg=ffffff",
        "https://target.scene7.com/is/image/Target/GUEST_1a31e61b-ca6d-4c7b-b862-fb337054d10d?wid=1000&hei=1000",
        "https://dynamic.indigoimages.ca//books/9780380788620.jpg?width=200&maxheight=200",
        "http://images.ecampus.com/images/d/8/620/9780380788620.jpg",
        "http://images.biggerbooks.com/images/d/8/620/9780380788620.jpg",
        "http://www0.alibris-static.com/isbn/9780380788620.gif",
        "http://www.printsasiaimages.com/524564465/13040502054755QJ2j9f.jpg",
        "http://images.knetbooks.com/images/d/8/620/9780380788620.jpg"
      ],
      "asin": "0380788624"
    }
  ]
}
 */

/*
https://api.upcitemdb.com/prod/trial/lookup?upc=025192345494
{
  "code": "OK",
  "total": 1,
  "offset": 0,
  "items": [
    {
      "ean": "0025192345494",
      "title": "Army Of Darkness (dvd)",
      "description": "The third in director Sam Raimi's stylish, comic book-like horror trilogy that began with The Evil Dead (1982), this tongue-in-cheek sequel offers equal parts sword-and-sorcery-style action, gore, and comedy. Bruce Campbell returns as the one-armed Ash, now a supermarket employee (\"Shop Smart. Shop S-Mart\") who is transported by the powers of a mysterious book back in time with his Oldsmobile '88 to the 14th century medieval era. Armed only with a shotgun, his high school chemistry textbook, and a chainsaw...",
      "upc": "025192345494",
      "brand": "ALLIANCE ENTERTAINMENT LLC",
      "model": "29504091",
      "color": "Gold",
      "size": "",
      "dimension": "7.5 X 0.7 X 5.4 inches",
      "weight": "0.2 Pounds",
      "category": "Media > DVDs & Videos",
      "currency": "",
      "lowest_recorded_price": 4.91,
      "highest_recorded_price": 9.99,
      "images": [
        "http://img.bbystatic.com/BestBuy_US/images/products/3040/30403783_sc.jpg",
        "https://target.scene7.com/is/image/Target/50793022?wid=1000&hei=1000",
        "http://images10.newegg.com/ProductImageCompressAll200/A17P_1_201608091357154973.jpg",
        "http://img1.r10.io/PIC/114122394/0/1/250/114122394.jpg",
        "http://www.fye.com/amgcover/dvd/full/w1/59/w15931hkl86.jpg",
        "http://8016235491c6828f9cae-6b0d87410f7cc1525cc32b79408788c4.r96.cf2.rackcdn.com/1979/83547352_1.jpg"
      ],
      "asin": "B01AHWLXDI",
      "elid": "112572700617"
    }
  ]
}
 */

/*
https://api.upcitemdb.com/prod/trial/lookup?upc=013131087130
{
  "code": "OK",
  "total": 1,
  "offset": 0,
  "items": [
    {
      "ean": "0013131087130",
      "title": "Army Of Darkness (vhs, 1999)",
      "description": "Justice & Brothers",
      "upc": "013131087130",
      "brand": "",
      "model": "",
      "color": "",
      "size": "",
      "dimension": "",
      "weight": "",
      "category": "Media > DVDs & Videos",
      "currency": "",
      "lowest_recorded_price": 0.99,
      "highest_recorded_price": 35,
      "images": [
        "http://images.alibris.com/cover/u38644w5n5s.jpg"
      ],
      "elid": "143520156740"
    }
  ]
}

 */