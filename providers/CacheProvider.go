package providers

import "github.com/hashicorp/golang-lru"


type CacheProvider struct {
	cache *lru.Cache
}

func NewCacheProvider(maxSize int) *CacheProvider {
	cache, _ := lru.New(maxSize)
	return &CacheProvider{
		cache: cache,
	}
}

func (cp *CacheProvider) GetItem(barcode string) *Item {
	item,ok := cp.cache.Get(barcode)
	if !ok {
		return nil
	}
	return item.(*Item)
}

func (cp *CacheProvider) AddToCache(barcode string, item *Item) {
	cp.cache.Add(barcode, item)
}