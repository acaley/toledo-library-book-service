package providers


type Item struct {
	Isbn13 string `json:"isbn13"`
	Upc string `json:"upc"`
	Title string `json:"title"`
	Creator string `json:"creator"`
	Image string `json:"image"`
	WebUrl string `json:"weburl"`
	ItemType int `json:"itemtype"`
}

type ItemProvider interface {
	GetItem(barcode string) *Item
	GetName() string
}