############################
# STEP 1 build executable binary
############################
FROM golang:1.13 AS builder

ADD . /go/src/gitlab.com/acaley/toledo-library-book-service

WORKDIR /go/src/gitlab.com/acaley/toledo-library-book-service

ENV CGO_ENABLED=0

RUN make deps
RUN make build

############################
# STEP 2 get certs
############################
FROM alpine:latest as certs
RUN apk --update add ca-certificates

############################
# STEP 3 build a small image
############################
FROM scratch
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

WORKDIR /app
# Copy our static executable.
COPY --from=builder /go/src/gitlab.com/acaley/toledo-library-book-service/toledo-library-book-service /app/toledo-library-book-service

# Run the  binary.
ENTRYPOINT ["./toledo-library-book-service"]

EXPOSE 8080
