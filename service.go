package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"gitlab.com/acaley/toledo-library-book-service/providers"
)

var itemProviders []providers.ItemProvider
var cacheProvider *providers.CacheProvider
var noItem = &providers.Item{}

func getItemFromLookupServices(barcode string) *providers.Item {
	var item *providers.Item
	item = cacheProvider.GetItem(barcode)
	if item != nil {
		log.Printf("Previously looked unfound item, found in cache:%s", barcode)
		if item == noItem {
			return nil
		}

		log.Printf("Item with barcode:%s found in cache", barcode)
		return item
	}

	for _, provider := range itemProviders {
		item = provider.GetItem(barcode)
		if item != nil {
			log.Printf("Item with barcode:%s found by provider:%s", barcode, provider.GetName())

			cacheProvider.AddToCache(barcode, item)
			return item
		}
	}

	cacheProvider.AddToCache(barcode, noItem)
	return nil
}

func getItemByBarcode(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	barcode := vars["barcode"]
	item := getItemFromLookupServices(barcode)
	if item == nil {
		w.WriteHeader(404)
		log.Printf("No item found for barcode:%s", barcode)
	} else {
		json.NewEncoder(w).Encode(item)
	}
}

func handleRequests() {
	r := mux.NewRouter()
	r.HandleFunc("/items/{barcode}", getItemByBarcode).Methods(http.MethodGet)
	log.Fatal(http.ListenAndServe(":8080", r))
}

func main() {
	cacheProvider = providers.NewCacheProvider(1000)

	itemProviders = append(itemProviders, providers.NewUpcItemDb())
	handleRequests()
}